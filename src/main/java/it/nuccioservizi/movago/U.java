package it.nuccioservizi.movago;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

/**
 * Utilità per semplificare la gestione dei null.
 */
@NonNullByDefault
public final class U {
    @SuppressWarnings("null")
	public static final Charset UTF8 = StandardCharsets.UTF_8;
    
	public static <T> List<T> asUnmodifiableList(final List<T> list) {
		@SuppressWarnings("null")
		@NonNull
		final List<T> u = Collections.unmodifiableList(list);

		return u;
	}

	public static <K, E> Map<K, E> asUnmodifiableMap(final Map<K, E> map) {
		@SuppressWarnings("null")
		@NonNull
		final Map<K, E> u = Collections.unmodifiableMap(map);

		return u;
	}

	public static String format(final String format, final Object... args) {
		final String s = String.format(format, args);
		Validate.notNull(s);
		return s;
	}

	public static <T> T ultimo(final List<T> l) {
		return l.get(l.size() - 1);
	}

	public static Path getParent(final Path path) {
		final Path p = path.getParent();
		if (p == null)
			throw new IllegalStateException("Parent null per: " + path);

		return p;
	}

	public static Path getPath(final String fileName) {
		final Path p = Paths.get(fileName);
		if (p == null)
			throw new IllegalStateException("Path null per: " + fileName);

		return p;
	}

	public static Path getPath(final Path dir, final String fileName) {
		final Path p = dir.resolve(fileName);
		if (p == null)
			throw new IllegalStateException("Path null in '" + dir + "' per: " + fileName);

		return p;
	}

	public static String getSystemProperty(final String property) {
		final String p = System.getProperty(property);
		if (p == null)
			throw new IllegalStateException("Property null: " + property);

		return p;
	}

	public static DirectoryStream<Path> newDirectoryStream(final Path dir, final String glob) throws IOException {
		@SuppressWarnings("null")
		final DirectoryStream<Path> s = Files.newDirectoryStream(dir, glob);
		Validate.notNull(s, "DirectoryStream null per cartella '%s' con glob: %s", dir, glob);

		return s;
	}

	public static BufferedReader newBufferedReader(final Path path, final Charset cs) throws IOException {
		final BufferedReader r = Files.newBufferedReader(path, cs);
		Validate.notNull(r, "BufferedReader null per: %s", path);

		return r;
	}

	public static BufferedWriter newBufferedWriter(final Path path, final Charset cs, final OpenOption... options)
			throws IOException {
		final BufferedWriter w = new BufferedWriter(new OutputStreamWriter(Files.newOutputStream(path, options), cs));
		Validate.notNull(w, "BufferedWriter null per: %s", path);

		return w;
	}

	public static OutputStream newOutputStream(final Path filePerG1) throws IOException {
		final OutputStream os = Files.newOutputStream(filePerG1);
		Validate.notNull(os, "Stream null per %s", filePerG1);

		return os;
	}

	public static int getInt(final CSVRecord record, final int index) {
		return Integer.parseInt(record.get(index));
	}

	@SuppressWarnings("null")
	public static String getString(final CSVRecord record, final int index) {
		return Validate.notNull(record.get(index));
	}

	public static String getStringIfExists(final CSVRecord record, final int index) {
		@SuppressWarnings("null")
		@NonNull
		final String s = record.size() < index + 1 ? "" : record.get(index);

		return s;
	}

	/**
	 * ATTENZIONE: Usare questo metodo solo se si serve UN SOLO
	 * {@link DocumentBuilder}, perché viene creata una
	 * {@link DocumentBuilderFactory} ad ogni esecuzione e questa è un'operazione
	 * costosa.
	 *
	 * @return
	 * @throws ParserConfigurationException
	 */
	public static DocumentBuilder newNonValidatingDocumentBuilder() throws ParserConfigurationException {
		final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setIgnoringComments(true);
		documentBuilderFactory.setValidating(false);
		@SuppressWarnings("null")
		@NonNull
		final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

		return documentBuilder;
	}

	public static Path getUserDir() {
		return getPath(getSystemProperty("user.dir"));
	}

	@SuppressWarnings("null")
	public static LocalDate now() {
		return LocalDate.now();
	}

	@SuppressWarnings("null")
	public static <T> Optional<T> optionalOf(final T o) {
		return Optional.of(o);
	}

	@SuppressWarnings("null")
	public static <T> Optional<T> optionalEmpty() {
		return Optional.empty();
	}

	@SuppressWarnings("null")
	public static <L, R> Pair<L, R> pairOf(final L left, final R right) {
		return ImmutablePair.of(left, right);
	}

	public static <L, R> L getLeft(final Pair<L, R> pair) {
		return pair.getLeft();
	}

	public static <L, R> R getRight(final Pair<L, R> pair) {
		return pair.getRight();
	}

	/**
	 * Torna il valore se è una partita IVA, cioè se è un numero di 11 cifre.
	 * Altrimenti torna una stringa vuota. Serve perché nel file di input capita di
	 * avere il CF nel campo sbagliato.
	 *
	 * @param cfOPiva il valore da filtrare.
	 * @return cfOPiva se è una partita IVA.
	 */
	public static String filtraCF(final String cfOPiva) {
		return cfOPiva.matches("^\\d{11}$") ? cfOPiva : ""; //$NON-NLS-1$ //$NON-NLS-2$
	}

	@SuppressWarnings("null")
	public static String estraiCodIva(final String codIvaENorma) {
		return codIvaENorma.substring(0, 2);
	}

	@SuppressWarnings("null")
	public static String estraiCodNormaIva(final String codIvaENorma) {
		return codIvaENorma.length() == 4 ? codIvaENorma.substring(2) : "  ";
	}

	@SuppressWarnings("null")
	public static String toUpperCase(final String string) {
		return string.toUpperCase();
	}

	public static String toString(@Nullable final Object obj) {
		@SuppressWarnings("null")
		@NonNull
		final String s = obj == null ? "" : obj.toString();
		return s;
	}

	public static <K, V> V mapGet(final Map<K, V> map, final K key, final V defaultValue) {
		return map.getOrDefault(key, defaultValue);
	}

}
