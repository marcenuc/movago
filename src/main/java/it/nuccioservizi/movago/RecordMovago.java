package it.nuccioservizi.movago;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Optional;
import java.util.TreeSet;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.Validate;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

@NonNullByDefault
public final class RecordMovago {

	private static final class CampoRecord implements Comparable<CampoRecord> {
		final String nome;
		final TipoCampo tipo;
		final int idxInizio;
		final int idxFine;
		final int lunghezza;
		final String valDefault;

		public CampoRecord(final String nome, final TipoCampo tipo, final int idxInizio, final int idxFine,
				final int lunghezza, final String valDefault) {
			Validate.isTrue(idxInizio > 0, "L'inizio del campo deve essere > 0: %d", idxInizio);
			Validate.isTrue(idxInizio <= idxFine, "La fine del campo deve essere >= dell'inizio: %d", idxFine);
			Validate.isTrue(lunghezza == (idxFine - idxInizio + 1),
					"La lunghezza del campo deve essere la distanza tra inizio e fine: %d", lunghezza);

			this.nome = nome;
			this.tipo = tipo;
			this.idxInizio = idxInizio;
			this.idxFine = idxFine;
			this.lunghezza = lunghezza;
			this.valDefault = valDefault;
		}

		@Override
		public String toString() {
			return "CampoRecord [nome=" + this.nome + ", tipo=" + this.tipo + ", idxInizio=" + this.idxInizio
					+ ", idxFine=" + this.idxFine + ", lunghezza=" + this.lunghezza + ", valDefault=" + this.valDefault
					+ "]";
		}

		@Override
		public int compareTo(final CampoRecord other) {
			return this.idxInizio < other.idxInizio //
					? -1 //
					: this.idxInizio > other.idxInizio //
							? 1 //
							: this.idxFine > other.idxFine //
									? -1 //
									: this.idxFine < other.idxFine //
											? 1 //
											: this.nome.compareTo(other.nome);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.idxFine;
			result = prime * result + this.idxInizio;
			result = prime * result + this.nome.hashCode();
			return result;
		}

		@Override
		public boolean equals(@Nullable final Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final CampoRecord other = (CampoRecord) obj;
			if (this.idxFine != other.idxFine)
				return false;
			if (this.idxInizio != other.idxInizio)
				return false;
			if (!this.nome.equals(other.nome))
				return false;
			return true;
		}

	}

	private final NavigableSet<CampoRecord> campi;

	public RecordMovago(final NavigableSet<CampoRecord> campi) {
		this.campi = campi;
	}

	@Override
	public String toString() {
		return "RecordMovago [campi=" + this.campi + "]";
	}

	private static int csvGetInt(final CSVRecord record, final String nomeCampo) {
		final String valore = record.get(nomeCampo);
		if (!valore.matches("^[0-9]+$"))
			throw new IllegalArgumentException(
					String.format("Il campo \"%s\" deve essere numerico, ma contiene \"%s\" nel record %s", nomeCampo,
							valore, record));

		return Integer.parseInt(valore);
	}

	private static String csvGetString(final CSVRecord record, final String nomeCampo) {
		final String valore = record.get(nomeCampo);
		if (valore == null || valore.trim().isEmpty())
			throw new IllegalArgumentException(
					String.format("Il campo \"%s\" è richiesto ma manca nel record %s", nomeCampo, record));

		@SuppressWarnings("null")
		@NonNull
		String trimmed = valore.trim();
		return trimmed;
	}

	private static String nomeFileCampiRecord(final String nomeApp, final String tipoRecord) {
		return nomeApp + "/Record" + tipoRecord + ".txt";
	}

	private static NavigableSet<CampoRecord> caricaCampi(final String nomeApp, final String tipoRecord)
			throws Exception {
		final NavigableSet<CampoRecord> campi = new TreeSet<>();

		try (final InputStream is = RecordMovago.class.getResourceAsStream(nomeFileCampiRecord(nomeApp, tipoRecord)); //
				final Reader in = new InputStreamReader(is, StandardCharsets.UTF_8); //
				final CSVParser records = CSVFormat.TDF.withFirstRecordAsHeader().parse(in)) {
			for (final CSVRecord record : records) {
				Validate.notNull(record);
				final String nome = csvGetString(record, "Nome campo");
				final TipoCampo tipo = TipoCampo.of(csvGetString(record, "Tipo"));
				final int idxInizio = csvGetInt(record, "Iniziale");
				final int idxFine = csvGetInt(record, "Finale");
				final int lunghezza = csvGetInt(record, "Lung");
				final String valDefault = U.toString(record.get("Default"));

				final CampoRecord campo;
				try {
					campo = new CampoRecord(nome, tipo, idxInizio, idxFine, lunghezza, valDefault);
				} catch (final IllegalArgumentException ex) {
					throw new IllegalArgumentException("Errore nel record " + record, ex);
				}
				campi.add(campo);
			}
		}

		CampoRecord campoPrecedente = null;
		for (final CampoRecord campo : campi) {
			if (campoPrecedente != null)
				if (campoPrecedente.idxInizio == campo.idxInizio) {
					/*
					 * Se due campi hanno lo stesso inizio, allora sono campi alternativi, cioè uno
					 * esclude l'altro. Il primo dei due deve essere lungo almeno quanto il secondo
					 * e, se più lungo, deve finire come uno dei campi successivi. In altre parole,
					 * la lunghezza di un campo alternativo deve essere uguale alla somma delle
					 * lunghezze dei campi di cui è l'alternativa.
					 */
					if (campoPrecedente.idxFine > campo.idxFine) {
						boolean trovato = false;
						for (final CampoRecord campo2 : campi.tailSet(campo, false)) {
							if (campoPrecedente.idxFine == campo2.idxFine) {
								trovato = true;
								break;
							}
						}
						Validate.isTrue(trovato, "Il campo alternativo %s non si sovrappone ai campi successivi",
								campoPrecedente);
					}
					Validate.isTrue(campoPrecedente.idxFine >= campo.idxFine,
							"Il campo %s è più piccolo del campo successivo: %s", campoPrecedente, campo);
				} else {
					/*
					 * Se due campi successivi NON hanno lo stesso inizio, allora devono essere
					 * consecutivi.
					 */
					Validate.isTrue(campoPrecedente.idxFine + 1 == campo.idxInizio,
							"I seguenti campi non sono consecutivi: %s\n%s", campoPrecedente, campo);
				}
			campoPrecedente = campo;
		}

		return campi;
	}

	public static RecordMovago creaDaFileCampiRecord(final String nomeApp, final String tipoRecord) throws Exception {
		final RecordMovago recordMovago = new RecordMovago(caricaCampi(nomeApp, tipoRecord));

		// TODO: campi e nomiCampi contengono le stesse informazioni: usarne
		// solo uno.
		for (final CampoRecord campo : recordMovago.campi)
			recordMovago.nomiCampi.put(campo.nome, campo);

		return recordMovago;
	}

	private final Map<String, String> valori = new HashMap<>();
	private final Map<String, CampoRecord> nomiCampi = new HashMap<>();

	public RecordMovago resetValori() {
		this.valori.clear();

		return this;
	}

	@SuppressWarnings("null")
	public RecordMovago set(final String nomeCampo, final int valore) {
		return set(nomeCampo, Integer.toString(valore));
	}

	public RecordMovago set(final String nomeCampo, final Optional<LocalDate> valore) {
		return set(nomeCampo, TipoCampo.D.print(valore));
	}

	public RecordMovago set(final String nomeCampo, final String valore) {
		Validate.isTrue(this.nomiCampi.containsKey(nomeCampo), "Il campo %s è sconosciuto. Nomi validi: %s", nomeCampo,
				this.nomiCampi);

		@SuppressWarnings("null")
		final CampoRecord campo = this.nomiCampi.get(nomeCampo);

		if (valore.length() > campo.lunghezza)
			System.err.println(String.format(
					"ATTENZIONE: il valore \"%s\" verrà troncato perché troppo lungo per il campo %s", valore, campo));

		this.valori.put(nomeCampo, valore);

		return this;
	}

	private @Nullable CampoRecord campoSuccessivo(final CampoRecord campo) {
		return this.campi.higher(campo);
	}

	public String print() {
		final StringBuilder str = new StringBuilder();
		int prossimoIdxInizio = 1;

		for (final CampoRecord campo : this.campi) {
			if (prossimoIdxInizio == campo.idxInizio) {
				if (!this.valori.containsKey(campo.nome)) {
					final CampoRecord prossimoCampo = campoSuccessivo(campo);
					if (prossimoCampo != null && prossimoCampo.idxInizio == campo.idxInizio)
						continue;
				}
				str.append(campo.tipo.print(this.valori.getOrDefault(campo.nome, campo.valDefault), campo.lunghezza));

				prossimoIdxInizio += campo.lunghezza;
			}
		}

		@SuppressWarnings("null")
		@NonNull
		final String s = str.toString();
		return s;
	}

	public static String printI(final RecordMovago i, final int numImportazione, final LocalDate dataImportazione) {
		return i.resetValori() //
				.set("NumSped", numImportazione) //
				.set("DataSped", U.optionalOf(dataImportazione)) //
				.print();
	}

	public static String printC(final RecordMovago rC, final int numImportazione, final LocalDate dataImportazione) {
		return rC.resetValori() //
				.set("NumSped", numImportazione) //
				.set("DataSped", U.optionalOf(dataImportazione)) //
				.print();
	}

	public static String printD10(final RecordMovago d10, final String denominazione, final String cognome,
			final String nome, final String codiceFiscale, final boolean isItaliano, final boolean isSocietà,
			final String codiceGestionale) {
		d10.resetValori();

		if (isSocietà)
			d10.set("SGXDENMN", denominazione);
		else
			d10.set("SGXCOGN", cognome) //
					.set("SGXNOME", nome);
		if (isItaliano)
			d10.set("SGCFISC", codiceFiscale);

		d10 //
				.set("SGCNATSOG", isItaliano ? "001" : "003") //
				.set("SGCTIPOANAG", isSocietà ? "SOC" : "PER") //
				.set("Codice_gestionale", codiceGestionale);

		return d10.print();
	}

	public static String printD11(final RecordMovago d11, final boolean isItaliano, final String indirizzo,
			final String numeroCivico, final String codComune, final String capComune, final String partitaIva,
			final String codiceFiscale, final Optional<LocalDate> dataDiNascita, final String codIsoEstero,
			final String sesso) {
		d11.resetValori();

		if (!isItaliano)
			d11.set("SGCIDENVFISCEST", codiceFiscale);

		return d11 //
				.set("INXINDR", indirizzo) //
				.set("INNCIV", numeroCivico) //
				.set("INCCOM", codComune) //
				.set("INCCAP", capComune) //
				.set(isItaliano ? "SGCPARTIVA" : "SGCNUMIDENVIVAEST", partitaIva) //
				.set("SGSNASC", dataDiNascita) //
				.set("SGCCODISOEST", codIsoEstero) //
				.set("SGCSES", sesso) //
				.print();
	}

	public static String printD30(final RecordMovago d30, final String codCausale, final int codAttivitàIva,
			final int codSezionaleIva, final int numFattura, final Optional<LocalDate> giorno,
			final String codClienteGestionale, final int totDocumento, final int progressivo) {
		return d30.resetValori() //
				.set("FACCAUS", codCausale) //
				.set("FANATTVIVA", codAttivitàIva) //
				.set("FACSEZNIVA", codSezionaleIva) //
				.set("FANPROT", numFattura) //
				.set("FANDOCM", numFattura) //
				.set("FADREGSN", giorno) //
				.set("FADDOCM", giorno) //
				.set("FACSOGGCLINFORN", codClienteGestionale) //
				.set("FAITOTDOCM", totDocumento) //
				.set("FANREGSN", progressivo) //
				.print();
	}

	public static String printD31(final RecordMovago d31, final String codIva, final int imponibile, final int imposta,
			final String codNorma, final String codContropartita) {
		return d31.resetValori() //
				.set("FACIVA", codIva) //
				.set("FAIIMPN", imponibile) //
				.set("FAIMP", imposta) //
				.set("FACNOR", codNorma) //
				.set("CSotConCosRic_suggest", codContropartita) //
				.print();
	}

	public static String printD40(final RecordMovago d40, final Optional<LocalDate> giorno, final String codIva,
			final int importo, final int rfDalNum, final int rfAlNum, final String codNorma, final String codConto,
			final int progressivo) {
		return d40.resetValori() //
				.set("CODREGSN", giorno) //
				.set("COCIVA", codIva) //
				.set("SEGIMP", importo < 0 ? "-" : "") //
				.set("COIOPER", importo < 0 ? -importo : importo) //
				.set("CONCORRVDAL", rfDalNum) //
				.set("CONCORRVAL", rfAlNum) //
				.set("COCNOR", codNorma) //
				.set("CSotConRic_suggest", codConto) //
				.set("CONREGSN", progressivo) //
				.print();
	}

	public static String printD50(final RecordMovago d50, final Optional<LocalDate> giorno,
			final String descrOperazione, final int importo, final String codContoDare, final String codContoAvere,
			final String codSottocontoDare, final String codSottocontoAvere, final String codContoCliente,
			final String clienteOFornitore, final int progressivo) {
		return d50.resetValori() //
				.set("PNDREGSN", giorno) //
				.set("PNXOPER", descrOperazione.isEmpty() ? "INCASSO" : descrOperazione) //
				.set("PNIOPER", importo) //
				.set("CSotConDare_suggest", codSottocontoDare) //
				.set("CSotConAve_suggest", codSottocontoAvere) //
				.set("PNCTIPOCLINFORN", clienteOFornitore) //
				.set("PNCSOGGCLINFORNDARE", codContoDare) //
				.set("PNCSOGGCLINFORNAVE", codContoAvere) //
				.set("CSotConAbbn_suggest", codContoCliente) //
				.set("PNNREGSN", progressivo) //
				.print();
	}
}
