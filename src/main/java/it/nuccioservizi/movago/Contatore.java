package it.nuccioservizi.movago;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * Wrapper per un valore intero che può essere letto solo incrementandolo.
 * Questo per risolvere due problemi: evitare l'errore di usare due volte lo
 * stesso valore; permettere di passarlo per riferimento così da poter essere
 * incrementato anche dal chiamante.
 *
 * La necessità nasce dal requisito imposto da AGO di avere un progressivo
 * comune a tutti i record. Non avendo tale progressivo nel file di input,
 * bisogna crearlo in fase di lettura del file da convertire.
 *
 * È richiesto un file in cui salvare il valore per non doverlo chiedere tutte
 * le volte all'utente.
 */
@NonNullByDefault
final class Contatore implements AutoCloseable {
    private final Path savePath;
    private int v;

    private Contatore(final Path savePath, final int v) {
        this.savePath = savePath;
        this.v = v;
    }

    public static Contatore fromFile(final Path inPath) throws IOException {
        if (!Files.exists(inPath))
            return new Contatore(inPath, 0);

        if (Files.isRegularFile(inPath))
            for (final String inLine : Files.readAllLines(inPath,
                    StandardCharsets.UTF_8))
                if (inLine.matches("^\\s*[0-9]+\\s*$"))
                    return new Contatore(inPath,
                            Integer.parseInt(inLine.trim()));

        throw new IllegalArgumentException(String.format(
                "'%s' deve essere un file e contenere solo un numero intero.",
                inPath));
    }

    private void toFile(final Path outPath) throws IOException {
        try (final BufferedWriter out = Files.newBufferedWriter(outPath)) {
            out.write(Integer.toString(this.v));
            out.newLine();
        }
    }

    @Override
    public void close() throws Exception {
        toFile(this.savePath);
    }

    public synchronized int succ() {
        return ++this.v;
    }
}
