"Nome campo"	"Tipo"	"Iniziale"	"Finale"	"Lung"	"Default"	"Descrizione"
"CodTipoRec"	"CHR"	1	1	1	"D"	"Contiene il carattere “D”"
"CodTipoDett"	"Num"	2	3	2	31	"Tipo dettaglio=31"
"FACIVA"	"Chr"	4	5	2		"Codice iva da tabella"
"Filler1"	"Filler"	6	9	4		
"FAIIMPN"	"Num"	10	25	16		"Imponibile"
"SEGIIMP"	"Chr"	26	26	1		"Segno imponibile (impostare solo se negativo, non impostare per le note di accredito, è la causale che imposta il segno)"
"Filler2"	"Filler"	27	30	4		
"FAIMP"	"Num"	31	46	16		"Imposta"
"SEGIMP"	"Num"	47	47	1		"Segno imposta (impostare solo se negativo  non impostare per le note di accredito, è la causale che imposta il segno)"
"FACNOR"	"Chr"	48	49	2		"Codice norma (da tabella Ago) (obbligatorio solo per alcuni codici iva)"
"CACCENCOS"	"Chr"	50	52	3		"Codice centro di costo"
"CSotConCosRic_suggest"	"Chr"	53	61	9		"Contropartita"
"FACOPERINTRR"	"Chr"	62	63	2		"Codice CEE (da tabella Ago)"
"FACTIPOOPERBLALIST"	"Chr"	64	66	3		"Solo per i clienti black list. Valori ammessi: 1=Beni 2=Servizi"
"FADCOMPRIGADA"	"D"	67	74	8		"Data competenza di rigo da"
"FADCOMPRIGAA"	"D"	75	82	8		"Data competenza di rigo a"
"FAFESLRIGASPL"	"Chr"	83	83	1		"Nel caso la fattura sia soggetta a split payment ma si voglia escludere una riga dall'applicazione."
"Filler3"	"Filler"	84	500	417		
