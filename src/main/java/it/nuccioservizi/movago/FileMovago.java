package it.nuccioservizi.movago;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

@NonNullByDefault
final class FileMovago implements AutoCloseable {
	@SuppressWarnings("null")
	private static final Charset CHARSET = StandardCharsets.ISO_8859_1;

	private final Contatore contatore;
	private int progressivo;
	private final RecordMovago rI;
	private final RecordMovago rD10;
	private final RecordMovago rD11;
	private final RecordMovago rD30;
	private final RecordMovago rD31;
	private final RecordMovago rD40;
	private final RecordMovago rD50;
	private final RecordMovago rC;
	private Optional<BufferedWriter> out;
	private Optional<Path> outPath;

	private FileMovago(final Contatore contatore, final int progressivo, final RecordMovago rI, final RecordMovago rD10,
			final RecordMovago rD11, final RecordMovago rD30, final RecordMovago rD31, final RecordMovago rD40,
			final RecordMovago rD50, final RecordMovago rC, final Optional<BufferedWriter> out,
			final Optional<Path> outPath) {
		this.contatore = contatore;
		this.progressivo = progressivo;
		this.rI = rI;
		this.rD10 = rD10;
		this.rD11 = rD11;
		this.rD30 = rD30;
		this.rD31 = rD31;
		this.rD40 = rD40;
		this.rD50 = rD50;
		this.rC = rC;
		this.out = out;
		this.outPath = outPath;
	}

	static FileMovago fromAppAndCounter(final String nomeApp, final Contatore contatore) throws Exception {
		return new FileMovago( //
				contatore, //
				-1, //
				RecordMovago.creaDaFileCampiRecord(nomeApp, "I"), //
				RecordMovago.creaDaFileCampiRecord(nomeApp, "D10"), //
				RecordMovago.creaDaFileCampiRecord(nomeApp, "D11"), //
				RecordMovago.creaDaFileCampiRecord(nomeApp, "D30"), //
				RecordMovago.creaDaFileCampiRecord(nomeApp, "D31"), //
				RecordMovago.creaDaFileCampiRecord(nomeApp, "D40"), //
				RecordMovago.creaDaFileCampiRecord(nomeApp, "D50"), //
				RecordMovago.creaDaFileCampiRecord(nomeApp, "C"), //
				U.optionalEmpty(), //
				U.optionalEmpty());
	}

	@SuppressWarnings("null")
	private Path getPath() {
		return U.getPath(String.format("MOVAGO-%d.txt", Integer.valueOf(this.progressivo)));
	}

	Optional<Path> getOutPath() {
		return this.outPath;
	}

	@SuppressWarnings({ "null", "resource" })
	private BufferedWriter out() throws IOException {
		final BufferedWriter o;
		
		if (this.out.isPresent())
			o = this.out.get();
		else {
			this.progressivo = this.contatore.succ();

			final Path p = getPath();
			o = U.newBufferedWriter(p, CHARSET, StandardOpenOption.CREATE_NEW);
			
			this.out = U.optionalOf(o);
			this.outPath = U.optionalOf(p);
		}
		
		return o;
	}

	@SuppressWarnings("null")
	@Override
	public void close() throws Exception {
		if (this.out.isPresent())
			this.out.get().close();
	}

	public void printI(final LocalDate dataImportazione) throws IOException {
		out().write(RecordMovago.printI(this.rI, this.progressivo, dataImportazione));
	}

	public void printC(final LocalDate dataImportazione) throws IOException {
		out().write(RecordMovago.printC(this.rC, this.progressivo, dataImportazione));
	}

	public void printD10(final String denominazione, final String cognome, final String nome,
			final String codiceFiscale, final boolean isItaliano, final boolean isSocietà,
			final String codiceGestionale) throws IOException {
		out().write(RecordMovago.printD10(this.rD10, denominazione, cognome, nome, codiceFiscale, isItaliano, isSocietà,
				codiceGestionale));
	}

	public void printD11(final boolean isItaliano, final String indirizzo, final String numeroCivico,
			final String codComune, final String capComune, final String partitaIva, final String codiceFiscale,
			final Optional<LocalDate> dataDiNascita, final String codIsoEstero, final String sesso) throws IOException {
		out().write(RecordMovago.printD11(this.rD11, isItaliano, indirizzo, numeroCivico, codComune, capComune,
				partitaIva, codiceFiscale, dataDiNascita, codIsoEstero, sesso));
	}

	public void printD30(final String codCausale, final int codAttivitàIva, final int codSezionaleIva,
			final int numFattura, final Optional<LocalDate> giorno, final String codClienteGestionale,
			final int totDocumento, final int progressivo2) throws IOException {
		out().write(RecordMovago.printD30(this.rD30, codCausale, codAttivitàIva, codSezionaleIva, numFattura, giorno,
				codClienteGestionale, totDocumento, progressivo2));
	}

	public void printD31(final String codIva, final int imponibile, final int imposta, final String codNorma,
			final String codContropartita) throws IOException {
		out().write(RecordMovago.printD31(this.rD31, codIva, imponibile, imposta, codNorma, codContropartita));
	}

	public void printD40(final Optional<LocalDate> giorno, final String codIva, final int importo, final int rfDalNum,
			final int rfAlNum, final String codNorma, final String codConto, final int succ) throws IOException {
		out().write(
				RecordMovago.printD40(this.rD40, giorno, codIva, importo, rfDalNum, rfAlNum, codNorma, codConto, succ));
	}

	public void printD50(final Optional<LocalDate> giorno, final String descrOperazione, final int importo,
			final String codContoDare, final String codContoAvere, final String codSottocontoDare,
			final String codSottocontoAvere, final String codContoCliente, final String clienteOFornitore,
			final int progressivoRecord) throws IOException {
		out().write(RecordMovago.printD50(this.rD50, giorno, descrOperazione, importo, codContoDare, codContoAvere,
				codSottocontoDare, codSottocontoAvere, codContoCliente, clienteOFornitore, progressivoRecord));
	}

}
