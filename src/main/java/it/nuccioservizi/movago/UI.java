package it.nuccioservizi.movago;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

@NonNullByDefault
public final class UI {
    private static final String MESSAGE_TITLE = "Welcome2any";

    public static void info(final String message) {
        JOptionPane
                .showMessageDialog(null, message, MESSAGE_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }

    public static void error(final String message) {
        JOptionPane.showMessageDialog(null, message, MESSAGE_TITLE, JOptionPane.ERROR_MESSAGE);
    }

    public static void info(final StringBuilder messageBuilder) {
        @SuppressWarnings("null")
        @NonNull
        final String message = messageBuilder.toString();
        UI.info(message);
    }

    public static void error(final Exception e) {
        @SuppressWarnings("null")
        @NonNull
        final String message = e.toString();
        error(message);
    }

    public static boolean sìNo(final String domanda) {
        return JOptionPane.showConfirmDialog(
                null, domanda, MESSAGE_TITLE, JOptionPane.YES_NO_OPTION
        ) == JOptionPane.YES_OPTION;
    }

    public static void init() throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
}
