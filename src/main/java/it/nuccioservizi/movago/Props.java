package it.nuccioservizi.movago;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import org.eclipse.jdt.annotation.NonNullByDefault;

@NonNullByDefault
public final class Props {
	public static Properties load(final Path propsPath) throws IOException {
		if (!Files.exists(propsPath)) {
			// Manca il file di configurazione:
			// lo creiamo copiando quello di esempio e usciamo.
			final String esempioPropsPath = "esempio-" + propsPath.getFileName();
			try (final InputStream in = Props.class.getResourceAsStream(esempioPropsPath)) {
				if (in == null)
					throw new RuntimeException("Manca configurazione di default: " + esempioPropsPath);
				Files.copy(in, propsPath);
			}
			UI.info("<html>È stato creato il file di configurazione<br><pre>" + propsPath
					+ "</pre><br>Modificarlo per le proprie esigenze e rilanciare il comando.</html>");
			System.exit(0);
		}

		final Properties props = new Properties();
		try (final Reader propsReader = U.newBufferedReader(propsPath, U.UTF8)) {
			props.load(propsReader);
		}

		return props;
	}

	public static String getString(final Properties props, final String key) {
		final String value = props.getProperty(key);

		if (value == null)
			throw new RuntimeException("Aggiungere " + key + " nel file di configurazione.");

		return value;
	}

	public static Path getPath(final Properties props, final String propertyName) {
		return U.getPath(getString(props, propertyName));
	}

	public static int getInt(final Properties props, final String propertyName) {
		return Integer.parseInt(getString(props, propertyName));
	}

}
