package it.nuccioservizi.movago;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

import org.apache.commons.lang3.Validate;
import org.eclipse.jdt.annotation.NonNullByDefault;

@NonNullByDefault
public enum TipoCampo {
	CHR {
		@SuppressWarnings("null")
		@Override
		public String print(final String valore, final int lunghezza) {
			return valore.length() >= lunghezza //
					? valore.substring(0, lunghezza) //
					: String.format("%-" + lunghezza + "s", valore);
		}

	},
	NUM {
		@SuppressWarnings("null")
		@Override
		public String print(final String valore, final int lunghezza) {
			Validate.isTrue(valore.length() <= lunghezza, "Numero troppo grande: %s", valore);

			return valore.isEmpty() //
					? String.format("%" + lunghezza + "s", "") //
					: String.format("%0" + lunghezza + "d", Integer.valueOf(valore));
		}

		@SuppressWarnings("null")
		@Override
		public String print(final int valore, final int lunghezza) {
			return print(Integer.toString(valore), lunghezza);
		}

	},
	FILLER {
		@SuppressWarnings("null")
		@Override
		public String print(final String valore, final int lunghezza) {
			return String.format("%" + lunghezza + "s", "");
		}

	},
	D {
		@SuppressWarnings("null")
		private final DateTimeFormatter FORMATTER = DateTimeFormatter.BASIC_ISO_DATE;
		private final String NESSUN_GIORNO = "        ";

		@SuppressWarnings("null")
		private String format(final LocalDate data) {
			return data.format(this.FORMATTER);
		}

		@Override
		public String print(final String valore, final int lunghezza) {
			if (valore.trim().isEmpty())
				return this.NESSUN_GIORNO;

			try {
				LocalDate.parse(valore, this.FORMATTER);
			} catch (final DateTimeParseException ex) {
				throw new IllegalArgumentException(String.format("'%s' non è una data valida.", valore), ex);
			}

			return valore;
		}

		@Override
		public String print(final Optional<LocalDate> data) {
			return data.isPresent() ? format(data.get()) : this.NESSUN_GIORNO;
		}
	};

	public abstract String print(String valore, int lunghezza);

	@SuppressWarnings("static-method")
	public String print(@SuppressWarnings("unused") final Optional<LocalDate> data) {
		throw new UnsupportedOperationException();
	}

	@SuppressWarnings("static-method")
	public String print(@SuppressWarnings("unused") final int numero, @SuppressWarnings("unused") final int lunghezza) {
		throw new UnsupportedOperationException();
	}

	public static TipoCampo of(final String mixedCaseName) {
		return TipoCampo.valueOf(U.toUpperCase(mixedCaseName));
	}
}
