# MOVAGO

Crea file in formato MOVAGO, per l'importazione in AGO di Zucchetti, a partire
da vari formati di esportazione di programmi gestionali.

Per ora, i formati di input supportati sono i file di esportazione dei seguenti
gestionali:

- [Welcome](http://www.larasoftware.com/)
- [DirectHoliday](http://directholiday.it/)

Il programma funziona così:

1. metti i file da trasformare nella stessa cartella del programma:
    * Welcome: il file xml chiamalo ContEst.XML
    * DirectHoliday: i nomi dei file hanno il formato specificato in `direct_holiday.properties`
2. esegui il programma, vedrai che crea due file con i progressivi
3. modifica i due file per metterci i valori corretti
4. rilancia il programma per creare il file MOVAGO-`progressivo`.txt
5. ora i file col progressivo dovrebbero essere aggiornati con l'ultimo progressivo usato
