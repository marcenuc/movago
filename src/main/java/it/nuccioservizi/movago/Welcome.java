package it.nuccioservizi.movago;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import nu.xom.Node;

@NonNullByDefault
public final class Welcome {
	@SuppressWarnings("null")
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("ddMMyyyy");

	public static LocalDate parseLocalDate(final String welcomeDate) {
		@SuppressWarnings("null")
		@NonNull
		final LocalDate date = LocalDate.parse(welcomeDate, DATE_FORMATTER);

		return date;
	}

	public static Optional<LocalDate> parseOptionalLocalDate(final String dateValue) {
		if (dateValue.isEmpty())
			return U.optionalEmpty();

		return U.optionalOf(parseLocalDate(dateValue));
	}

	public static final String XPATH_ANTICIPO_GIORNO = "Anticipi/Anticipo";
	public static final String XPATH_FATTURA_GIORNO = "FattureEmesse/FatturaEmessa";
	public static final String XPATH_DATA = "Data";
	public static final String XPATH_GIORNO = "/Corrispettivi/Giorno";
	public static final String XPATH_CODICE = "Codice";
	public static final String XPATH_CLIENTE = "/Corrispettivi/Clienti/Cliente";
	public static final String XPATH_CODICE_CLIENTE = "CodiceCliente";
	public static final String XPATH_CAUSALE = "Causale";
	public static final String XPATH_FATTURA = "/Corrispettivi/Giorno/FattureEmesse/FatturaEmessa";

	public static final class TipoFattura {
		public final String suffisso;
		public final String causale;
		public final int attivitàIva;
		public final int sezionaleIva;

		public TipoFattura(final String suffisso, final String causale, final int attivitàIva, final int sezionaleIva) {
			this.suffisso = suffisso;
			this.causale = causale;
			this.attivitàIva = attivitàIva;
			this.sezionaleIva = sezionaleIva;
		}
	}

	private final TipoFattura fattura;
	private final TipoFattura notaDiCredito;
	private final TipoFattura fatturaLocazione;

	public Welcome(final TipoFattura fattura, final TipoFattura notaDiCredito, final TipoFattura fatturaLocazione) {
		this.fattura = fattura;
		this.notaDiCredito = notaDiCredito;
		this.fatturaLocazione = fatturaLocazione;
	}

	public TipoFattura toTipoFattura(final boolean isNotaDiCredito, final List<Node> contropartite,
			final Set<String> contiDiRicavoFattureDiLocazione) throws Exception {
		if (isNotaDiCredito)
			return this.notaDiCredito;

		@SuppressWarnings("null")
		final Node primaContropartita = contropartite.get(0);
		final String contoPrimaContropartita = X.getNodeText(primaContropartita, "Codice");

		if (contiDiRicavoFattureDiLocazione.contains(contoPrimaContropartita))
			return this.fatturaLocazione;

		return this.fattura;
	}

}
