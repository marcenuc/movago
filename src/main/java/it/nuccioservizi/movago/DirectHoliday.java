package it.nuccioservizi.movago;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * Converte file Direct Holiday in MOVAGO per l'importazione in Zucchetti AGO.
 *
 */
@NonNullByDefault
public final class DirectHoliday {
    @SuppressWarnings("null")
	private static final String NOME_APP = DirectHoliday.class.getSimpleName()
            .toLowerCase();

    @SuppressWarnings("null")
	private static final CSVFormat CSV_FORMAT = CSVFormat.newFormat(';')
            .withIgnoreEmptyLines().withTrim();
    @SuppressWarnings("null")
	private static final Charset DH_CHARSET = StandardCharsets.ISO_8859_1;
    private static final String CONFIG_FILE_NAME = "direct_holiday.properties";

    private final Path cartellaDiLavoro;

    public DirectHoliday(final Path cartellaDiLavoro) {
        this.cartellaDiLavoro = cartellaDiLavoro;
    }

    public static void main(final String[] args) throws Exception {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            final DirectHoliday app;

            if (args.length == 0) {
                final Path appPath = Paths
                        .get(DirectHoliday.class.getProtectionDomain()
                                .getCodeSource().getLocation().toURI());
                app = new DirectHoliday((Files.isRegularFile(appPath) ? appPath
                        : appPath.getParent()).getParent());
            } else
                app = new DirectHoliday(U.getPath(args[0]));

            final Cfg cfg = Cfg
                    .load(U.getPath(app.cartellaDiLavoro, CONFIG_FILE_NAME));

            final Map<String, CSVRecord> clientiImportazione = new HashMap<>();
            final List<DocumentoDh> documentiImportazione = new ArrayList<>();
            final List<Path> fileLetti = new ArrayList<>();

            try (final DirectoryStream<Path> dhPathsStream = U
                    .newDirectoryStream(app.cartellaDiLavoro,
                            cfg.getFiltroNomiFileDh())) {
                for (final Path fileDh : dhPathsStream) {
                    try (final Reader dhReader = Files.newBufferedReader(fileDh,
                            DH_CHARSET)) {

                        // I record di un documento hanno lo stesso progressivo.
                        String progressivoDocumento = null;
                        DocumentoDh documentoDh = null;

                        for (final CSVRecord record : CSV_FORMAT
                                .parse(dhReader)) {
                            final String progressivoRecord = RecordDh
                                    .getProgressivo(record);

                            if (progressivoDocumento != null
                                    && documentoDh != null
                                    && progressivoDocumento
                                            .equals(progressivoRecord))
                                documentoDh.addRecord(record);
                            else {
                                documentoDh = DocumentoDh.of(record);
                                documentiImportazione.add(documentoDh);
                                progressivoDocumento = progressivoRecord;
                            }

                            if ((documentoDh.isFattura()
                                    || documentoDh.isNotaDiCredito())
                                    && RecordDh.isCliente(record))
                                clientiImportazione.put(
                                        RecordDh.getCodCliente(record), record);
                        }
                    }
                    fileLetti.add(fileDh);
                }
            }

            final LocalDate dataImportazione = U.now();
            Optional<String> outPath = Optional.empty();

            try (final Contatore progressivoImportazioni = Contatore
                    .fromFile(U.getPath("progressivo_importazioni.txt"));
                    final Contatore progressivo = Contatore
                            .fromFile(U.getPath("progressivo_record.txt"));
                    final FileMovago out = FileMovago.fromAppAndCounter(
                            NOME_APP, progressivoImportazioni)) {

                out.printI(dataImportazione);

                for (final CSVRecord cliente : clientiImportazione.values())
                    clienteToRecordD10D11(out, cliente);

                for (final DocumentoDh documentoDh : documentiImportazione)
                    if (documentoDh.isFattura()
                            || documentoDh.isNotaDiCredito())
                        fatturaONotaDiCreditoToRecordD30D31(cfg, out,
                                documentoDh, progressivo.succ());

                for (final DocumentoDh documentoDh : documentiImportazione)
                    if (documentoDh.isRicevutaFiscale())
                        corrispettivoToRecordD40(out, documentoDh, progressivo);

                for (final DocumentoDh documentoDh : documentiImportazione)
                    if (documentoDh.isFattura()
                            || documentoDh.isNotaDiCredito())
                        fatturaToRecordD50(cfg, out, documentoDh, progressivo);
                    else if (documentoDh.isRicevutaFiscale())
                        pagamentiECaparreToRecordD50(cfg, out, documentoDh,
                                progressivo);
                    else if (documentoDh.isRecordInserimentoCaparra())
                        anticipoToRecordD50(cfg, out, documentoDh, progressivo);
                    else
                        throw new IllegalStateException(
                                "Tipo documento sconosciuto: "
                                        + documentoDh.getTipoDocumento());

                out.printC(dataImportazione);

                outPath = U.optionalOf(out.getOutPath().toString());
            }

            {
                final StringBuilder report = new StringBuilder("<html>");
                report.append(
                        "<h1>Conversione da Direct Holiday a Gestionale1 completata</h1>") //
                        .append("<dl><dt>Cartella di lavoro:</dt><dd><p>") //
                        .append(app.cartellaDiLavoro) //
                        .append("</p></dd><dt>File letti:</dt><dd><ol>");
                for (final Path f : fileLetti)
                    report.append("<li>").append(f.getFileName())
                            .append("</li>");
                report.append("</ol></dd>") //
                        .append("<dt>File generato:</dt><dd><p>")
                        .append(outPath.orElse("")).append("</p></dd>") //
                        .append("</dl>") //
                        .append("</html>");

                DirectHoliday.info(report.toString());
            }

        } catch (final Exception e) {
            DirectHoliday.error(e.getClass().getSimpleName() + ": "
                    + e.getLocalizedMessage());
            throw e;
        }
    }

    private static final String MESSAGE_TITLE = "DH to MOVAGO";

    public static void info(final String message) {
        JOptionPane.showMessageDialog(null, message, MESSAGE_TITLE,
                JOptionPane.INFORMATION_MESSAGE);
    }

    public static void error(final String message) {
        JOptionPane.showMessageDialog(null, message, MESSAGE_TITLE,
                JOptionPane.ERROR_MESSAGE);
    }

    @SuppressWarnings("null")
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter
            .ofPattern("dd/MM/uuuu");

    private static Optional<LocalDate> getDate(final String dateValue) {
        return dateValue.isEmpty() ? U.optionalEmpty()
                : U.optionalOf(LocalDate.parse(dateValue, DATE_FORMATTER));
    }

    private static void clienteToRecordD10D11(final FileMovago out,
            final CSVRecord cliente) throws IOException {
        final String denominazione = cliente.get(4);
        final String cognome = cliente.get(11);
        final String nome = cliente.get(12);
        final String codiceFiscale = DocumentoDh.getCodFiscale(cliente);
        final String codiceGestionale = cliente.get(3);

        final String indirizzo = cliente.get(5);
        final String numeroCivico = "";
        final String codComune = cliente.get(15);
        final String capComune = cliente.get(6);
        final String partitaIva = DocumentoDh.getPartitaIva(cliente);
        final Optional<LocalDate> dataDiNascita = getDate(cliente.get(17));
        final String codIsoEstero = cliente.get(13);
        final String sesso = "";

        final boolean isItaliano = "IT".equals(codIsoEstero);
        final boolean isSocietà = "".equals(cognome) && "".equals(nome);

        out.printD10(denominazione, cognome, nome, codiceFiscale, isItaliano,
                isSocietà, codiceGestionale);
        out.printD11(isItaliano, indirizzo, numeroCivico, codComune, capComune,
                partitaIva, codiceFiscale, dataDiNascita, codIsoEstero, sesso);
    }

    private static int importoInCentesimi(final String importo) {
        return Integer.parseInt(importo.replace(".", "").replaceFirst(",", ""));
    }

    private static void fatturaONotaDiCreditoToRecordD30D31(final Cfg cfg,
            final FileMovago out, final DocumentoDh documentoDh,
            final int progressivo) throws Exception {
        final LocalDate data = documentoDh.getDataDocumento();
        final int numdoc = documentoDh.getNumDocumento();
        final Optional<LocalDate> giorno = Optional.of(data);
        final String codClienteGestionale = documentoDh.getCodClienteDh();

        if (documentoDh.isRecordDocumento()) {
            final String codCausale;
            final int codAttivitàIva;
            final int codSezionaleIva;
            if (documentoDh.isFattura()) {
                codCausale = cfg.getCausaleFattura();
                codAttivitàIva = cfg.getAttivitàIvaFattura();
                codSezionaleIva = cfg.getSezionaleIvaFattura();
            } else {
                codCausale = cfg.getCausaleNotaDiCredito();
                codAttivitàIva = cfg.getAttivitàIvaNotaDiCredito();
                codSezionaleIva = cfg.getSezionaleIvaNotaDiCredito();
            }
            out.printD30(codCausale, codAttivitàIva, codSezionaleIva, numdoc,
                    giorno, codClienteGestionale,
                    importoInCentesimi(documentoDh.getImporto()), progressivo);
        }

        for (final CSVRecord record : documentoDh.getRecords())
            if (RecordDh.isRiga(record)) {
                final int imponibile = importoInCentesimi(record.get(5));
                final int imposta = importoInCentesimi(record.get(6));
                final String codIvaENorma = record.get(4);
                final String codIva = U.estraiCodIva(codIvaENorma);
                final String codNorma = U.estraiCodNormaIva(codIvaENorma);
                final String codContropartita = record.get(3);

                out.printD31(codIva, imponibile, imposta, codNorma,
                        codContropartita);
            }
    }

    private static void corrispettivoToRecordD40(final FileMovago out,
            final DocumentoDh documentoDh, final Contatore progressivo)
            throws Exception {
        final Optional<LocalDate> data = Optional
                .of(documentoDh.getDataDocumento());

        for (final CSVRecord record : documentoDh.getRecords())
            if (RecordDh.isRiga(record) && documentoDh.isRecordDocumento()) {
                final String codIvaENorma = record.get(4);
                final String codIva = U.estraiCodIva(codIvaENorma);
                final String codNorma = U.estraiCodNormaIva(codIvaENorma);
                final int imponibile = importoInCentesimi(record.get(5));
                final int imposta = importoInCentesimi(record.get(6));
                final int importo = imponibile + imposta;
                final String codConto = record.get(3);
                final int rfDalNum = documentoDh.getNumDocumento();
                final int rfAlNum = rfDalNum;

                out.printD40(data, codIva, importo, rfDalNum, rfAlNum, codNorma,
                        codConto, progressivo.succ());
            }
    }

    private static int getImporto(final CSVRecord record) {
        final int colonna = RecordDh.isPagamento(record) ? 3 : 4;
        return importoInCentesimi(record.get(colonna));
    }

    private static String getContoDare(final Cfg cfg, final CSVRecord record) {
        return RecordDh.isRestituzioneCaparra(record) ? cfg.getContoCaparra()
                : record.get(4);
    }

    private static void pagamentiECaparreToRecordD50(final Cfg cfg,
            final FileMovago out, final DocumentoDh documentoDh,
            final Contatore progressivo) throws Exception {
        final Optional<LocalDate> data = Optional
                .of(documentoDh.getDataDocumento());
        final String descrOperazione = "RF " + documentoDh.getNumDocumento()
                + (data.isPresent() ? " " + data.get() : "");
        final String codContoSottocontoDare2 = "";
        final String codContoSottocontoAvere1 = "";
        final String codContoClientiRicevuteFiscali = cfg
                .getContoClientiRicevuteFiscali();
        final String codCliente = "";
        final String codContoCliente = "";
        final String clienteOFornitore = "";

        for (final CSVRecord record : documentoDh.getRecords())
            if (RecordDh.isPagamento(record)
                    || RecordDh.isRestituzioneCaparra(record)) {
                final int importoConSegno = getImporto(record);
                if (importoConSegno == 0)
                    continue;

                final String codContoSottocontoAvere2;
                final String codContoSottocontoDare1;
                final int importo;
                if (importoConSegno >= 0) {
                    importo = importoConSegno;
                    codContoSottocontoAvere2 = codContoClientiRicevuteFiscali;
                    codContoSottocontoDare1 = getContoDare(cfg, record);
                } else {
                    importo = -importoConSegno;
                    codContoSottocontoAvere2 = getContoDare(cfg, record);
                    codContoSottocontoDare1 = codContoClientiRicevuteFiscali;
                }

                final int p = progressivo.succ();

                out.printD50(data, descrOperazione, importo, codCliente,
                        codCliente, codContoSottocontoDare1,
                        codContoSottocontoAvere1, codContoCliente,
                        clienteOFornitore, p);
                out.printD50(data, descrOperazione, importo, codCliente,
                        codCliente, codContoSottocontoDare2,
                        codContoSottocontoAvere2, codContoCliente,
                        clienteOFornitore, p);
            }
    }

    private static void fatturaToRecordD50(final Cfg cfg, final FileMovago out,
            final DocumentoDh documentoDh, final Contatore progressivo)
            throws Exception {
        final Optional<LocalDate> data = Optional
                .of(documentoDh.getDataDocumento());
        final int numdoc = documentoDh.getNumDocumento();
        final boolean notaDiCredito = documentoDh.isNotaDiCredito();
        final String descrOperazione = (notaDiCredito ? "NC " : "FT ") + numdoc
                + (data.isPresent() ? " " + data.get() : "");
        final String codContoAvere = documentoDh.getCodClienteDh();
        final String codCliente = "";
        final String clienteOFornitore = "001";
        final String codContoClienti = cfg.getContoClientiFatture();
        for (final CSVRecord record : documentoDh.getRecords())
            if (RecordDh.isPagamento(record)
                    || RecordDh.isRestituzioneCaparra(record)) {
                final int importo = getImporto(record);
                if (importo == 0)
                    continue;
                final String codSottocontoDare = getContoDare(cfg, record);
                final int p = progressivo.succ();

                out.printD50(data, descrOperazione, importo, "", codContoAvere,
                        codCliente, codCliente, codContoClienti,
                        clienteOFornitore, p);
                out.printD50(data, descrOperazione, importo, "", "",
                        codSottocontoDare, codCliente, "", "", p);
            }
    }

    private static void anticipoToRecordD50(final Cfg cfg, final FileMovago out,
            final DocumentoDh documentoDh, final Contatore progressivo)
            throws Exception {
        final Optional<LocalDate> data = Optional
                .of(documentoDh.getDataDocumento());
        final String descrOperazione = documentoDh.getClienteCaparra();
        final String codContoDare = "";
        final String codContoAvere = "";
        final String codContoCaparra = cfg.getContoCaparra();

        for (final CSVRecord record : documentoDh.getRecords())
            if (RecordDh.isPagamento(record)) {
                final int importoConSegno = getImporto(record);

                final String codSottocontoDare;
                final String codSottocontoAvere;
                final int importo;
                if (importoConSegno >= 0) {
                    codSottocontoDare = getContoDare(cfg, record);
                    codSottocontoAvere = codContoCaparra;
                    importo = importoConSegno;
                } else {
                    codSottocontoDare = codContoCaparra;
                    codSottocontoAvere = getContoDare(cfg, record);
                    importo = -importoConSegno;
                }

                final int p = progressivo.succ();

                out.printD50(data, descrOperazione, importo, codContoDare,
                        codContoAvere, codSottocontoDare, "", "", "", p);
                out.printD50(data, descrOperazione, importo, codContoDare,
                        codContoAvere, "", codSottocontoAvere, "", "", p);
            }
    }

    private static final class Cfg {
        private final Properties props;

        public Cfg(final Properties props) {
            this.props = props;
        }

        public static Cfg load(final Path propsPath) throws IOException {
            if (!Files.exists(propsPath)) {
                // Manca il file di configurazione:
                // lo creiamo copiando quello di esempio e usciamo.
                final String esempioPropsPath = "esempio-"
                        + propsPath.getFileName();
                try (final InputStream in = DirectHoliday.class
                        .getResourceAsStream(esempioPropsPath)) {
                    if (in == null)
                        throw new RuntimeException(
                                "Manca configurazione di default: "
                                        + esempioPropsPath);
                    Files.copy(in, propsPath);
                }
                DirectHoliday.info("<html>"
                        + "È stato creato il file di configurazione.<br>"
                        + "<pre>" + propsPath + "</pre><br>"
                        + "Modificarlo per le proprie esigenze"
                        + " e rilanciare il programma." //
                        + "</html>");
                System.exit(0);
            }

            final Properties props = new Properties();
            try (final Reader propsReader = Files.newBufferedReader(propsPath,
                    StandardCharsets.UTF_8)) {
                props.load(propsReader);
            }

            return new Cfg(props);
        }

        public String getFiltroNomiFileDh() {
            return this.props.getProperty("filtro_nomi_file_dh");
        }

        public String getContoCaparra() {
            return this.props.getProperty("conto_caparra");
        }

        public String getContoClientiRicevuteFiscali() {
            return this.props.getProperty("conto_clienti_ricevute_fiscali");
        }

        public String getContoClientiFatture() {
            return this.props.getProperty("conto_clienti_fatture");
        }

        public String getCausaleCaparra() {
            return this.props.getProperty("causale_caparra");
        }

        public String getCausaleRicevutaFiscale() {
            return this.props.getProperty("causale_ricevuta_fiscale");
        }

        public String getCausaleNotaDiCredito() {
            return this.props.getProperty("causale_nota_di_credito");
        }

        public String getCausaleFattura() {
            return this.props.getProperty("causale_fattura");
        }

        public int getAttivitàIvaRicevutaFiscale() {
            return Integer.parseInt(
                    this.props.getProperty("attività_iva_ricevuta_fiscale"));
        }

        public int getAttivitàIvaNotaDiCredito() {
            return Integer.parseInt(
                    this.props.getProperty("attività_iva_nota_di_credito"));
        }

        public int getAttivitàIvaFattura() {
            return Integer
                    .parseInt(this.props.getProperty("attività_iva_fattura"));
        }

        public int getSezionaleIvaRicevutaFiscale() {
            return Integer.parseInt(
                    this.props.getProperty("sezionale_iva_ricevuta_fiscale"));
        }

        public int getSezionaleIvaNotaDiCredito() {
            return Integer.parseInt(
                    this.props.getProperty("sezionale_iva_nota_di_credito"));
        }

        public int getSezionaleIvaFattura() {
            return Integer
                    .parseInt(this.props.getProperty("sezionale_iva_fattura"));
        }

    }

    private static final class RecordDh {
        private static final String TIPO_RECORD_DH_CLIENTE = "A";
        private static final String TIPO_RECORD_DH_RIGA = "R";
        private static final String TIPO_RECORD_DH_PAGAMENTO = "P";
        private static final String TIPO_RECORD_DH_RESTITUZIONE_CAPARRA = "C";

        private static final int COL_TIPO_RECORD = 1;
        private static final int COL_PROGRESSIVO = 2;

        private static final int COL_A_COD_CLIENTE_DH = 3;

        public static String tipoRecord(final CSVRecord record) {
            return record.get(COL_TIPO_RECORD);
        }

        public static boolean isCliente(final CSVRecord record) {
            return tipoRecord(record).equals(TIPO_RECORD_DH_CLIENTE);
        }

        public static boolean isRiga(final CSVRecord record) {
            return tipoRecord(record).equals(TIPO_RECORD_DH_RIGA);
        }

        public static boolean isPagamento(final CSVRecord record) {
            return tipoRecord(record).equals(TIPO_RECORD_DH_PAGAMENTO);
        }

        public static boolean isRestituzioneCaparra(final CSVRecord record) {
            return tipoRecord(record)
                    .equals(TIPO_RECORD_DH_RESTITUZIONE_CAPARRA);
        }

        public static String getProgressivo(final CSVRecord record) {
            return record.get(COL_PROGRESSIVO);
        }

        public static String getCodCliente(final CSVRecord record) {
            return record.get(COL_A_COD_CLIENTE_DH);
        }
    }

    private static final class DocumentoDh {
        private static final String TIPO_DOC_FATTURA = "F";
        private static final String TIPO_DOC_NOTA_CREDITO = "N";
        private static final String TIPO_DOC_RICEVUTA_FISCALE = "R";

        private static final int COL_TIPO_RECORD = 1;

        private static final int COL_T_TIPO_DOC_DH = 3;
        private static final int COL_T_DATA_DOC_DH = 6;
        private static final int COL_T_NUM_DOC_DH = 8;
        private static final int COL_T_IMPORTO_DOC_DH = 7;

        private static final int COL_A_COD_CLIENTE_DH = 3;
        private static final int COL_A_PARTITA_IVA = 9;
        private static final int COL_A_COD_FISCALE = 10;

        private static final int COL_D_DATA_DOC_DH = 5;
        private static final int COL_D_CLIENTE_DH = 6;

        private static final String TIPO_RECORD_DH_DOCUMENTO = "T";
        private static final String TIPO_RECORD_DH_INCASSO = "I";
        private static final String TIPO_RECORD_DH_INSERIMENTO_CAPARRA = "D";

        private static final String[] TIPI_RECORD_DH = {
                TIPO_RECORD_DH_DOCUMENTO, TIPO_RECORD_DH_INCASSO,
                TIPO_RECORD_DH_INSERIMENTO_CAPARRA };
        static {
            // ATTENZIONE: TIPI_RECORD_DH deve essere ordinato per usare
            // Arrays.binarySearch()
            Arrays.sort(TIPI_RECORD_DH);
        }

        private final String tipoDocumento;
        private final String tipoRecord;
        private final List<CSVRecord> records = new ArrayList<>();

        public static DocumentoDh of(final CSVRecord testata) {
            if (Arrays.binarySearch(TIPI_RECORD_DH, tipoRecord(testata)) < 0)
                throw new IllegalStateException(
                        "Il primo record deve essere la testata (tipi "
                                + Arrays.toString(TIPI_RECORD_DH)
                                + "), invece è: " + tipoRecord(testata) + ":\n"
                                + testata);

            return new DocumentoDh(testata);
        }

        private static String tipoRecord(final CSVRecord record) {
            return RecordDh.tipoRecord(record);
        }

        private DocumentoDh(final CSVRecord testata) {
            this.tipoDocumento = testata.get(COL_T_TIPO_DOC_DH);
            this.tipoRecord = testata.get(COL_TIPO_RECORD);
            addRecord(testata);
        }

        public void addRecord(final CSVRecord record) {
            this.records.add(record);
        }

        public String getTipoDocumento() {
            return this.tipoDocumento;
        }

        public boolean isFattura() {
            return this.tipoDocumento.equals(TIPO_DOC_FATTURA);
        }

        public boolean isNotaDiCredito() {
            return this.tipoDocumento.equals(TIPO_DOC_NOTA_CREDITO);
        }

        public boolean isRicevutaFiscale() {
            return this.tipoDocumento.equals(TIPO_DOC_RICEVUTA_FISCALE);
        }

        public boolean isRecordDocumento() {
            return TIPO_RECORD_DH_DOCUMENTO.equals(this.tipoRecord);
        }

        public boolean isRecordIncasso() {
            return TIPO_RECORD_DH_INCASSO.equals(this.tipoRecord);
        }

        public boolean isRecordInserimentoCaparra() {
            return TIPO_RECORD_DH_INSERIMENTO_CAPARRA.equals(this.tipoRecord);
        }

        public CSVRecord getTestata() {
            return this.records.get(0);
        }

        private CSVRecord getCliente() {
            return this.records.get(1);
        }

        public List<CSVRecord> getRecords() {
            return this.records;
        }

        private int getColonnaDataDocumento() {
            return isRecordInserimentoCaparra() ? COL_D_DATA_DOC_DH
                    : COL_T_DATA_DOC_DH;
        }

        public LocalDate getDataDocumento() {
            return toDate(getTestata().get(getColonnaDataDocumento()));
        }

        public int getNumDocumento() {
            return Integer.parseInt(getTestata().get(COL_T_NUM_DOC_DH));
        }

        public String getCodClienteDh() {
            return getCliente().get(COL_A_COD_CLIENTE_DH);
        }

        public String getImporto() {
            return getTestata().get(COL_T_IMPORTO_DOC_DH);
        }

        public String getClienteCaparra() {
            return getTestata().get(COL_D_CLIENTE_DH);
        }

        public static String getCodFiscale(final CSVRecord recordCliente) {
            return recordCliente.get(COL_A_COD_FISCALE);
        }

        public static String getPartitaIva(final CSVRecord recordCliente) {
            return recordCliente.get(COL_A_PARTITA_IVA);
        }

        private static LocalDate toDate(final String dataDh) {
            return LocalDate.parse(dataDh, DateTimeFormatter.BASIC_ISO_DATE);
        }
    }
}
