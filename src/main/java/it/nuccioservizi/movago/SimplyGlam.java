package it.nuccioservizi.movago;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import nu.xom.Document;
import nu.xom.Node;

@NonNullByDefault
public final class SimplyGlam {

	@SuppressWarnings("null")
	private static final String NOME_APP = SimplyGlam.class.getSimpleName().toLowerCase();

	private static final String FILTRO_XML = "*.[xX][mM][lL]";

	public static void main(final String[] args) throws Exception {
		try {
			UI.init();
			final List<Path> fileLetti = new ArrayList<>();
			try (final Contatore progressivoImportazioni = Contatore
					.fromFile(U.getPath("progressivo_importazioni.txt"));
					final Contatore progressivo = Contatore.fromFile(U.getPath("progressivo_record.txt"));
					final FileMovago out = FileMovago.fromAppAndCounter(NOME_APP, progressivoImportazioni)) {
				final Path inDir = U.getUserDir();

				try (final DirectoryStream<Path> welcomePaths = U.newDirectoryStream(inDir, FILTRO_XML)) {
					for (final Path welcomePath : welcomePaths) {
						if (!UI.sìNo("Leggo il file " + welcomePath + "?"))
							continue;

						final Document in = X.newDocument(welcomePath);
						fileLetti.add(welcomePath);

						final LocalDate dataImportazione = U.now();

						out.printI(dataImportazione);

						for (final Node cliente : X.queryNodes(in, Welcome.XPATH_CLIENTE))
							clienteToRecordD10D11(out, cliente);

						for (final Node giorno : X.queryNodes(in, Welcome.XPATH_GIORNO)) {
							final LocalDate data = Welcome.parseLocalDate(X.getNodeText(giorno, Welcome.XPATH_DATA));

							for (final Node fattura : X.queryNodes(giorno, Welcome.XPATH_FATTURA_GIORNO)) {
								fatturaToRecordD30(out, data, fattura, progressivo.succ());

								for (final Node contropartita : X.queryNodes(fattura, "Contropartite/Contropartita")) {
									contropartitaToRecordD31(out, contropartita);
								}
							}
						}

						for (final Node giorno : X.queryNodes(in, Welcome.XPATH_GIORNO)) {
							final LocalDate data = Welcome.parseLocalDate(X.getNodeText(giorno, Welcome.XPATH_DATA));

							for (final Node corrispettivo : X.queryNodes(giorno, "Ricevute/RepartoCorrispettivi")) {
								corrispettivoToRecordD40(out, data, corrispettivo, progressivo);
							}
						}

						for (final Node giorno : X.queryNodes(in, Welcome.XPATH_GIORNO)) {
							final LocalDate data = Welcome.parseLocalDate(X.getNodeText(giorno, Welcome.XPATH_DATA));

							for (final Node pagamentoRicevute : X.queryNodes(giorno, "Ricevute/Pagamento"))
								pagamentoToRecordD50(out, data, pagamentoRicevute, progressivo.succ());

							for (final Node fattura : X.queryNodes(giorno, Welcome.XPATH_FATTURA_GIORNO)) {
								final String codContoAvere = X.getNodeText(fattura, "CodiceCliente");

								for (final Node pagamentoFattura : X.queryNodes(fattura, "Pagamento"))
									fatturaToRecordD50(out, data, pagamentoFattura, codContoAvere, progressivo.succ());
							}

							for (final Node anticipo : X.queryNodes(giorno, "Anticipi/Anticipo"))
								anticipoToRecordD50(out, data, anticipo, progressivo.succ());
						}

						out.printC(dataImportazione);
					}
				}

				{
					final StringBuilder report = new StringBuilder("<html>");
					report.append("<h1>Conversione completata</h1>") //
							.append("<dl><dt>Cartella di lavoro:</dt><dd><p>") //
							.append(inDir) //
							.append("</p></dd><dt>File letti:</dt><dd><ol>");
					for (final Path f : fileLetti)
						report //
								.append("<li>") //
								.append(f.getFileName()) //
								.append("</li>");
					report.append("</ol></dd><dt>File generato:</dt><dd><p>") //
							.append(out.getOutPath().map(p -> p.toString()).orElseGet(() -> "NESSUN FILE GENERATO")) //
							.append("</p></dd></dl>") //
							.append("</html>");

					UI.info(report);
				}
			}
		} catch (final Exception e) {
			UI.error(e);
			throw e;
		}
	}

	/**
	 * Torna il valore se è una partita IVA, cioè se è un numero di 11 cifre.
	 * Altrimenti torna una stringa vuota. Serve perché nel file di input capita di
	 * avere il CF nel campo sbagliato.
	 *
	 * @param cfOPiva il valore da filtrare.
	 * @return cfOPiva se è una partita IVA.
	 */
	private static String filtraCF(final String cfOPiva) {
		return cfOPiva.matches("^\\d{11}$") ? cfOPiva : "";
	}

	private static void clienteToRecordD10D11(final FileMovago out, final Node cliente) throws IOException {
		final String denominazione = X.getNodeText(cliente, "RagioneSociale");
		final String cognome = X.getNodeText(cliente, "Cognome");
		final String nome = X.getNodeText(cliente, "Nome");
		final String codiceFiscale = X.getNodeText(cliente, "CodiceFiscale");
		final boolean isItaliano = "IT".equals(X.getNodeText(cliente, "SiglaNazione"));
		final boolean isSocietà = "N".equals(X.getNodeText(cliente, "PersonaFisica"));
		final String codiceGestionale = X.getNodeText(cliente, "Codice");

		final String indirizzo = X.getNodeText(cliente, "Indirizzo");
		final String numeroCivico = "";
		final String codComune = "";
		final String capComune = X.getNodeText(cliente, "Cap");
		final String partitaIva = filtraCF(X.getNodeText(cliente, "PartitaIVA"));
		final Optional<LocalDate> dataDiNascita = Welcome.parseOptionalLocalDate(X.getNodeText(cliente, "DataNascita"));
		final String codIsoEstero = X.getNodeText(cliente, "SiglaNazione");
		final String sesso = "";

		out.printD10(denominazione, cognome, nome, codiceFiscale, isItaliano, isSocietà, codiceGestionale);
		out.printD11(isItaliano, indirizzo, numeroCivico, codComune, capComune, partitaIva, codiceFiscale,
				dataDiNascita, codIsoEstero, sesso);
	}

	private static void fatturaToRecordD30(final FileMovago out, final LocalDate data, final Node fattura,
			final int progressivo) throws Exception {
		final int numFattura = X.getNodeInt(fattura, "Numero");
		final Optional<LocalDate> giorno = U.optionalOf(data);
		final String codClienteGestionale = X.getNodeText(fattura, "CodiceCliente");
		final int totDocumento = X.getNodeCent(fattura, "TotaleDocumento");

		// TODO: definire causali, attività e sezionali iva per simplyglam
		out.printD30("TODO", -1, -1, numFattura, giorno, codClienteGestionale, totDocumento, progressivo);
	}

	private static void contropartitaToRecordD31(final FileMovago out, final Node contropartita) throws Exception {
		final String codIvaENorma = X.getNodeText(contropartita, "IvaNetto/Codice");
		final String codIva = U.estraiCodIva(codIvaENorma);
		final String codNorma = U.estraiCodNormaIva(codIvaENorma);
		final int imponibile = X.getNodeCent(contropartita, "IvaNetto/Imponibile");
		final int imposta = X.getNodeCent(contropartita, "IvaNetto/Imposta");
		final String codContropartita = X.getNodeText(contropartita, "Codice");

		out.printD31(codIva, imponibile, imposta, codNorma, codContropartita);
	}

	private static void corrispettivoToRecordD40(final FileMovago out, final LocalDate data, final Node corrispettivo,
			final Contatore progressivo) throws Exception {
		final Optional<LocalDate> giorno = U.optionalOf(data);
		final String codConto = X.getNodeText(corrispettivo, "CodiceConto");

		for (@NonNull
		final Node iva : X.queryNodes(corrispettivo, "IVA")) {
			final String codIvaENorma = X.getNodeText(iva, "Codice");
			final String codIva = U.estraiCodIva(codIvaENorma);
			final String codNorma = U.estraiCodNormaIva(codIvaENorma);
			final int importo = X.getNodeCent(iva, "Importo");

			out.printD40(giorno, codIva, importo, 0, 0, codNorma, codConto, progressivo.succ());
		}
	}

	private static final String codContoClienti = "102280000";

	private static void pagamentoToRecordD50(final FileMovago out, final LocalDate data, final Node pagamento,
			final int progressivo) throws Exception {
		final Optional<LocalDate> giorno = U.optionalOf(data);
		final String descrOperazione = "";
		final int importo = X.getNodeCent(pagamento, "Importo");
		final String codContoSottocontoDare1 = X.getNodeText(pagamento, "Codice");
		final String codContoSottocontoDare2 = "";
		final String codContoSottocontoAvere1 = "";
		final String codContoSottocontoAvere2 = "102330000";
		final String codCliente = "";
		final String codContoCliente = "";
		final String clienteOFornitore = "";

		out.printD50(giorno, descrOperazione, importo, codCliente, codCliente, codContoSottocontoDare1,
				codContoSottocontoAvere1, codContoCliente, clienteOFornitore, progressivo);
		out.printD50(giorno, descrOperazione, importo, codCliente, codCliente, codContoSottocontoDare2,
				codContoSottocontoAvere2, codContoCliente, clienteOFornitore, progressivo);
	}

	private static void fatturaToRecordD50(final FileMovago out, final LocalDate data, final Node pagamento,
			final String codContoAvere, final int progressivo) throws Exception {
		final Optional<LocalDate> giorno = U.optionalOf(data);
		final String descrOperazione = "";
		final int importo = X.getNodeCent(pagamento, "Importo");
		final String codSottocontoDare = X.getNodeText(pagamento, "Codice");
		final String codCliente = "";
		final String clienteOFornitore = "001";

		out.printD50(giorno, descrOperazione, importo, "", codContoAvere, codCliente, codCliente, codContoClienti,
				clienteOFornitore, progressivo);
		out.printD50(giorno, descrOperazione, importo, "", "", codSottocontoDare, codCliente, "", "", progressivo);
	}

	private static void anticipoToRecordD50(final FileMovago out, final LocalDate data, final Node anticipo,
			final int progressivo) throws Exception {
		final Optional<LocalDate> giorno = U.optionalOf(data);
		final String descrOperazione = X.getNodeText(anticipo, "Descrizione");
		final int importo = X.getNodeCent(anticipo, "Importo");
		final String codContoDare = "";
		final String codContoAvere = "";
		final String codSottocontoAvere = X.getNodeText(anticipo, "ContoAnticipo");
		final String codSottocontoDare = X.getNodeText(anticipo, "ContoPagamento");

		out.printD50(giorno, descrOperazione, importo, codContoDare, codContoAvere, codSottocontoDare, "", "", "",
				progressivo);
		out.printD50(giorno, descrOperazione, importo, codContoDare, codContoAvere, "", codSottocontoAvere, "", "",
				progressivo);
	}

}
