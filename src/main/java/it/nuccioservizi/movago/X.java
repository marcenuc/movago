package it.nuccioservizi.movago;

import java.io.IOException;
import java.nio.file.Path;

import org.apache.commons.lang3.Validate;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

@NonNullByDefault
final class X {
	private X() {
	}

	public static Document newDocument(final Path file) throws ValidityException, ParsingException, IOException {
		final Builder parser = new Builder();
		final Document doc = parser.build(file.toFile());
		Validate.notNull(doc);
		return doc;
	}

	@SuppressWarnings("null")
	public static Iterable<Node> queryNodes(final Node node, final String xpath) {
		return node.query(xpath);
	}

	public static String getNodeText(final Node node, final String xpath) {
		final Nodes nodes = node.query(xpath);
		if (nodes.size() < 1)
			return "";

		final String text = nodes.get(0).getValue();
		if (text == null)
			return "";

		@SuppressWarnings("null")
		@NonNull
		final String trimmed = text.trim();
		return trimmed;
	}

	public static int getNodeInt(final Node node, final String xpath) {
		return Integer.parseInt(getNodeText(node, xpath));
	}

	public static int getNodeCent(final Node node, final String xpath) {
		return Integer.parseInt(getNodeText(node, xpath).replaceFirst("\\.(\\d\\d)$", "$1"));
	}
}
